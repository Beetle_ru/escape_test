#!/bin/bash
clear
#Ключ -e, в команде echo, разрешает интерпретацию escape-последовательностей.
echo -e "\033[1mЭто жирный текст.\033[0m"
echo -e "\033[4mЭто подчеркнутый текст.\033[0m"
#echo -e '\E[COLOR1;COLOR2mКакой либо текст.'
for (( j=40; j<48; j++))
do
    echo -e -n  " $j "
done
for (( x=0; x<5; x++))
do
    echo ""
    for (( j=40; j<48; j++))
    do
        echo -e -n  "\E[${j}m    "
    done
done
tput sgr0 #возвращает настройки терминала в первоначальное состояние
echo ""
echo ""
for (( j=30; j<38; j++))
do
    echo -e -n  " $j "
done
for (( x=0; x<5; x++))
do
    echo ""
    for (( j=30; j<38; j++))
    do
        echo -e -n  "\E[${j}m@@@@"
    done
done
tput sgr0 #возвращает настройки терминала в первоначальное состояние
echo ""
exit 0 